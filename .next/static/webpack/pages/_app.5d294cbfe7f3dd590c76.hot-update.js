webpackHotUpdate_N_E("pages/_app",{

/***/ "./src/primary-nav.js":
/*!****************************!*\
  !*** ./src/primary-nav.js ***!
  \****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {var primaryNav = {
  "pages": [{
    "displayName": "Home",
    "path": "/"
  }, {
    "displayName": "Blog",
    "path": "/blog"
  }, {
    "displayName": "Contact",
    "path": "/contact"
  }]
};
/* harmony default export */ __webpack_exports__["default"] = (primaryNav);

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vc3JjL3ByaW1hcnktbmF2LmpzIl0sIm5hbWVzIjpbInByaW1hcnlOYXYiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQTtBQUFBLGtEQUFJQSxVQUFVLEdBQUc7QUFDZixXQUFTLENBQUM7QUFDUixtQkFBZSxNQURQO0FBRVIsWUFBUTtBQUZBLEdBQUQsRUFHUDtBQUNBLG1CQUFlLE1BRGY7QUFFQSxZQUFRO0FBRlIsR0FITyxFQU1QO0FBQ0EsbUJBQWUsU0FEZjtBQUVBLFlBQVE7QUFGUixHQU5PO0FBRE0sQ0FBakI7QUFhZUEseUVBQWYiLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvX2FwcC41ZDI5NGNiZmU3ZjNkZDU5MGM3Ni5ob3QtdXBkYXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsibGV0IHByaW1hcnlOYXYgPSB7XHJcbiAgXCJwYWdlc1wiOiBbe1xyXG4gICAgXCJkaXNwbGF5TmFtZVwiOiBcIkhvbWVcIixcclxuICAgIFwicGF0aFwiOiBcIi9cIlxyXG4gIH0se1xyXG4gICAgXCJkaXNwbGF5TmFtZVwiOiBcIkJsb2dcIixcclxuICAgIFwicGF0aFwiOiBcIi9ibG9nXCJcclxuICB9LHtcclxuICAgIFwiZGlzcGxheU5hbWVcIjogXCJDb250YWN0XCIsXHJcbiAgICBcInBhdGhcIjogXCIvY29udGFjdFwiXHJcbiAgfV1cclxufTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IHByaW1hcnlOYXY7Il0sInNvdXJjZVJvb3QiOiIifQ==