webpackHotUpdate_N_E("pages/_app",{

/***/ "./src/global-styles.js":
/*!******************************!*\
  !*** ./src/global-styles.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var _babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! styled-components */ "./node_modules/styled-components/dist/styled-components.browser.esm.js");
/* harmony import */ var _theme__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./theme */ "./src/theme.js");


function _templateObject() {
  var data = Object(_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n  // global css variables\n  html {\n    --color-primary: ", ";\n    --color-primary-contrast: ", ";\n    --color-secondary: ", ";\n    --color-secondary-contrast: ", ";\n    --color-background: ", ";\n    --color-background-contrast: ", ";\n  }\n\n  //other global styles go here...\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}



var GlobalStyles = Object(styled_components__WEBPACK_IMPORTED_MODULE_1__["createGlobalStyle"])(_templateObject(), _theme__WEBPACK_IMPORTED_MODULE_2__["default"].colors.primaryColor, _theme__WEBPACK_IMPORTED_MODULE_2__["default"].colors.colorPrimaryContrast, _theme__WEBPACK_IMPORTED_MODULE_2__["default"].colors.colorSecondary, _theme__WEBPACK_IMPORTED_MODULE_2__["default"].colors.colorSecondaryContrast, _theme__WEBPACK_IMPORTED_MODULE_2__["default"].colors.colorBackground, _theme__WEBPACK_IMPORTED_MODULE_2__["default"].colors.colorBackgroundContrast);
/* harmony default export */ __webpack_exports__["default"] = (GlobalStyles);

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./src/theme.js":
/*!**********************!*\
  !*** ./src/theme.js ***!
  \**********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {var theme = {
  colors: {
    //colors
    colorPrimary: 'green',
    colorPrimaryContrast: '#fff',
    colorSecondary: 'orange',
    colorSecondaryContrast: '#fff',
    colorTertiary: 'green',
    colorTertiaryContrast: '#fff',
    colorBackground: '#fff',
    colorBackroundContrast: '#333',
    colorDark: '#333'
  },
  space: {
    spaceSml: '5px',
    spaceMed: '10px'
  },
  fontSizes: {},
  fonts: {
    fontSans: 'sans-serif'
  },
  fontWeights: {},
  lineHeights: {},
  letterSpacings: {},
  sizes: {},
  borderWidths: {},
  borderStyles: {},
  radii: {},
  shadows: {// '$distant': '0 15px 35px -5px rgba(0,0,0,.25)',
  },
  zIndices: {},
  transitions: {// '$fast': 'all 50ms ease',
  }
};
/* harmony default export */ __webpack_exports__["default"] = (theme);

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vc3JjL2dsb2JhbC1zdHlsZXMuanMiLCJ3ZWJwYWNrOi8vX05fRS8uL3NyYy90aGVtZS5qcyJdLCJuYW1lcyI6WyJHbG9iYWxTdHlsZXMiLCJjcmVhdGVHbG9iYWxTdHlsZSIsInRoZW1lIiwiY29sb3JzIiwicHJpbWFyeUNvbG9yIiwiY29sb3JQcmltYXJ5Q29udHJhc3QiLCJjb2xvclNlY29uZGFyeSIsImNvbG9yU2Vjb25kYXJ5Q29udHJhc3QiLCJjb2xvckJhY2tncm91bmQiLCJjb2xvckJhY2tncm91bmRDb250cmFzdCIsImNvbG9yUHJpbWFyeSIsImNvbG9yVGVydGlhcnkiLCJjb2xvclRlcnRpYXJ5Q29udHJhc3QiLCJjb2xvckJhY2tyb3VuZENvbnRyYXN0IiwiY29sb3JEYXJrIiwic3BhY2UiLCJzcGFjZVNtbCIsInNwYWNlTWVkIiwiZm9udFNpemVzIiwiZm9udHMiLCJmb250U2FucyIsImZvbnRXZWlnaHRzIiwibGluZUhlaWdodHMiLCJsZXR0ZXJTcGFjaW5ncyIsInNpemVzIiwiYm9yZGVyV2lkdGhzIiwiYm9yZGVyU3R5bGVzIiwicmFkaWkiLCJzaGFkb3dzIiwiekluZGljZXMiLCJ0cmFuc2l0aW9ucyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBRUEsSUFBTUEsWUFBWSxHQUFHQywyRUFBSCxvQkFHS0MsOENBQUssQ0FBQ0MsTUFBTixDQUFhQyxZQUhsQixFQUljRiw4Q0FBSyxDQUFDQyxNQUFOLENBQWFFLG9CQUozQixFQUtPSCw4Q0FBSyxDQUFDQyxNQUFOLENBQWFHLGNBTHBCLEVBTWdCSiw4Q0FBSyxDQUFDQyxNQUFOLENBQWFJLHNCQU43QixFQU9RTCw4Q0FBSyxDQUFDQyxNQUFOLENBQWFLLGVBUHJCLEVBUWlCTiw4Q0FBSyxDQUFDQyxNQUFOLENBQWFNLHVCQVI5QixDQUFsQjtBQWNlVCwyRUFBZjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDakJBO0FBQUEsa0RBQU1FLEtBQUssR0FBRztBQUNWQyxRQUFNLEVBQUU7QUFDSjtBQUNBTyxnQkFBWSxFQUFFLE9BRlY7QUFHSkwsd0JBQW9CLEVBQUUsTUFIbEI7QUFJSkMsa0JBQWMsRUFBRSxRQUpaO0FBS0pDLDBCQUFzQixFQUFFLE1BTHBCO0FBTUpJLGlCQUFhLEVBQUUsT0FOWDtBQU9KQyx5QkFBcUIsRUFBRSxNQVBuQjtBQVFKSixtQkFBZSxFQUFFLE1BUmI7QUFTSkssMEJBQXNCLEVBQUUsTUFUcEI7QUFVSkMsYUFBUyxFQUFFO0FBVlAsR0FERTtBQWFWQyxPQUFLLEVBQUU7QUFDSEMsWUFBUSxFQUFFLEtBRFA7QUFFSEMsWUFBUSxFQUFFO0FBRlAsR0FiRztBQWlCVkMsV0FBUyxFQUFFLEVBakJEO0FBbUJWQyxPQUFLLEVBQUU7QUFDSEMsWUFBUSxFQUFFO0FBRFAsR0FuQkc7QUFzQlZDLGFBQVcsRUFBRSxFQXRCSDtBQXVCVkMsYUFBVyxFQUFFLEVBdkJIO0FBd0JWQyxnQkFBYyxFQUFFLEVBeEJOO0FBeUJWQyxPQUFLLEVBQUUsRUF6Qkc7QUEwQlZDLGNBQVksRUFBRSxFQTFCSjtBQTJCVkMsY0FBWSxFQUFFLEVBM0JKO0FBNEJWQyxPQUFLLEVBQUUsRUE1Qkc7QUE2QlZDLFNBQU8sRUFBRSxDQUNMO0FBREssR0E3QkM7QUFnQ1ZDLFVBQVEsRUFBRSxFQWhDQTtBQWlDVkMsYUFBVyxFQUFFLENBQ1Q7QUFEUztBQWpDSCxDQUFkO0FBc0NlNUIsb0VBQWYiLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvX2FwcC44YWNmMzZkYjZjMWY0NTYxOWUwNC5ob3QtdXBkYXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgY3JlYXRlR2xvYmFsU3R5bGUgfSBmcm9tICdzdHlsZWQtY29tcG9uZW50cyc7XHJcbmltcG9ydCB0aGVtZSBmcm9tICcuL3RoZW1lJztcclxuXHJcbmNvbnN0IEdsb2JhbFN0eWxlcyA9IGNyZWF0ZUdsb2JhbFN0eWxlYFxyXG4gIC8vIGdsb2JhbCBjc3MgdmFyaWFibGVzXHJcbiAgaHRtbCB7XHJcbiAgICAtLWNvbG9yLXByaW1hcnk6ICR7dGhlbWUuY29sb3JzLnByaW1hcnlDb2xvcn07XHJcbiAgICAtLWNvbG9yLXByaW1hcnktY29udHJhc3Q6ICR7dGhlbWUuY29sb3JzLmNvbG9yUHJpbWFyeUNvbnRyYXN0fTtcclxuICAgIC0tY29sb3Itc2Vjb25kYXJ5OiAke3RoZW1lLmNvbG9ycy5jb2xvclNlY29uZGFyeX07XHJcbiAgICAtLWNvbG9yLXNlY29uZGFyeS1jb250cmFzdDogJHt0aGVtZS5jb2xvcnMuY29sb3JTZWNvbmRhcnlDb250cmFzdH07XHJcbiAgICAtLWNvbG9yLWJhY2tncm91bmQ6ICR7dGhlbWUuY29sb3JzLmNvbG9yQmFja2dyb3VuZH07XHJcbiAgICAtLWNvbG9yLWJhY2tncm91bmQtY29udHJhc3Q6ICR7dGhlbWUuY29sb3JzLmNvbG9yQmFja2dyb3VuZENvbnRyYXN0fTtcclxuICB9XHJcblxyXG4gIC8vb3RoZXIgZ2xvYmFsIHN0eWxlcyBnbyBoZXJlLi4uXHJcbmA7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBHbG9iYWxTdHlsZXM7IiwiY29uc3QgdGhlbWUgPSB7XHJcbiAgICBjb2xvcnM6IHtcclxuICAgICAgICAvL2NvbG9yc1xyXG4gICAgICAgIGNvbG9yUHJpbWFyeTogJ2dyZWVuJyxcclxuICAgICAgICBjb2xvclByaW1hcnlDb250cmFzdDogJyNmZmYnLFxyXG4gICAgICAgIGNvbG9yU2Vjb25kYXJ5OiAnb3JhbmdlJyxcclxuICAgICAgICBjb2xvclNlY29uZGFyeUNvbnRyYXN0OiAnI2ZmZicsXHJcbiAgICAgICAgY29sb3JUZXJ0aWFyeTogJ2dyZWVuJyxcclxuICAgICAgICBjb2xvclRlcnRpYXJ5Q29udHJhc3Q6ICcjZmZmJyxcclxuICAgICAgICBjb2xvckJhY2tncm91bmQ6ICcjZmZmJyxcclxuICAgICAgICBjb2xvckJhY2tyb3VuZENvbnRyYXN0OiAnIzMzMycsXHJcbiAgICAgICAgY29sb3JEYXJrOiAnIzMzMydcclxuICAgIH0sXHJcbiAgICBzcGFjZToge1xyXG4gICAgICAgIHNwYWNlU21sOiAnNXB4JyxcclxuICAgICAgICBzcGFjZU1lZDogJzEwcHgnXHJcbiAgICB9LFxyXG4gICAgZm9udFNpemVzOiB7XHJcbiAgICB9LFxyXG4gICAgZm9udHM6IHtcclxuICAgICAgICBmb250U2FuczogJ3NhbnMtc2VyaWYnXHJcbiAgICB9LFxyXG4gICAgZm9udFdlaWdodHM6IHt9LFxyXG4gICAgbGluZUhlaWdodHM6IHt9LFxyXG4gICAgbGV0dGVyU3BhY2luZ3M6IHt9LFxyXG4gICAgc2l6ZXM6IHt9LFxyXG4gICAgYm9yZGVyV2lkdGhzOiB7fSxcclxuICAgIGJvcmRlclN0eWxlczoge30sXHJcbiAgICByYWRpaToge30sXHJcbiAgICBzaGFkb3dzOiB7XHJcbiAgICAgICAgLy8gJyRkaXN0YW50JzogJzAgMTVweCAzNXB4IC01cHggcmdiYSgwLDAsMCwuMjUpJyxcclxuICAgIH0sXHJcbiAgICB6SW5kaWNlczoge30sXHJcbiAgICB0cmFuc2l0aW9uczoge1xyXG4gICAgICAgIC8vICckZmFzdCc6ICdhbGwgNTBtcyBlYXNlJyxcclxuICAgIH0sXHJcbn07XHJcblxyXG5leHBvcnQgZGVmYXVsdCB0aGVtZTsiXSwic291cmNlUm9vdCI6IiJ9