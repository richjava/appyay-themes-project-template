webpackHotUpdate_N_E("pages/_app",{

/***/ "./src/global-styles.js":
/*!******************************!*\
  !*** ./src/global-styles.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var _babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! styled-components */ "./node_modules/styled-components/dist/styled-components.browser.esm.js");
/* harmony import */ var _theme__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./theme */ "./src/theme.js");


function _templateObject() {
  var data = Object(_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n  // global css variables\n  html {\n    --color-primary: ", ";\n    --color-primary-contrast: ", ";\n    --color-secondary: ", ";\n    --color-secondary-contrast: ", ";\n    --color-background: ", ";\n    --color-background-contrast: ", ";\n  }\n\n  //other global styles go here...\n  p {color:green}\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}



var GlobalStyles = Object(styled_components__WEBPACK_IMPORTED_MODULE_1__["createGlobalStyle"])(_templateObject(), _theme__WEBPACK_IMPORTED_MODULE_2__["default"].colors.primaryColor, _theme__WEBPACK_IMPORTED_MODULE_2__["default"].colors.colorPrimaryContrast, _theme__WEBPACK_IMPORTED_MODULE_2__["default"].colors.colorSecondary, _theme__WEBPACK_IMPORTED_MODULE_2__["default"].colors.colorSecondaryContrast, _theme__WEBPACK_IMPORTED_MODULE_2__["default"].colors.colorBackground, _theme__WEBPACK_IMPORTED_MODULE_2__["default"].colors.colorBackgroundContrast);
/* harmony default export */ __webpack_exports__["default"] = (GlobalStyles);

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vc3JjL2dsb2JhbC1zdHlsZXMuanMiXSwibmFtZXMiOlsiR2xvYmFsU3R5bGVzIiwiY3JlYXRlR2xvYmFsU3R5bGUiLCJ0aGVtZSIsImNvbG9ycyIsInByaW1hcnlDb2xvciIsImNvbG9yUHJpbWFyeUNvbnRyYXN0IiwiY29sb3JTZWNvbmRhcnkiLCJjb2xvclNlY29uZGFyeUNvbnRyYXN0IiwiY29sb3JCYWNrZ3JvdW5kIiwiY29sb3JCYWNrZ3JvdW5kQ29udHJhc3QiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUVBLElBQU1BLFlBQVksR0FBR0MsMkVBQUgsb0JBR0tDLDhDQUFLLENBQUNDLE1BQU4sQ0FBYUMsWUFIbEIsRUFJY0YsOENBQUssQ0FBQ0MsTUFBTixDQUFhRSxvQkFKM0IsRUFLT0gsOENBQUssQ0FBQ0MsTUFBTixDQUFhRyxjQUxwQixFQU1nQkosOENBQUssQ0FBQ0MsTUFBTixDQUFhSSxzQkFON0IsRUFPUUwsOENBQUssQ0FBQ0MsTUFBTixDQUFhSyxlQVByQixFQVFpQk4sOENBQUssQ0FBQ0MsTUFBTixDQUFhTSx1QkFSOUIsQ0FBbEI7QUFlZVQsMkVBQWYiLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvX2FwcC5iYzhhMmI5YzA0NzVkNjM3NmFmYS5ob3QtdXBkYXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgY3JlYXRlR2xvYmFsU3R5bGUgfSBmcm9tICdzdHlsZWQtY29tcG9uZW50cyc7XHJcbmltcG9ydCB0aGVtZSBmcm9tICcuL3RoZW1lJztcclxuXHJcbmNvbnN0IEdsb2JhbFN0eWxlcyA9IGNyZWF0ZUdsb2JhbFN0eWxlYFxyXG4gIC8vIGdsb2JhbCBjc3MgdmFyaWFibGVzXHJcbiAgaHRtbCB7XHJcbiAgICAtLWNvbG9yLXByaW1hcnk6ICR7dGhlbWUuY29sb3JzLnByaW1hcnlDb2xvcn07XHJcbiAgICAtLWNvbG9yLXByaW1hcnktY29udHJhc3Q6ICR7dGhlbWUuY29sb3JzLmNvbG9yUHJpbWFyeUNvbnRyYXN0fTtcclxuICAgIC0tY29sb3Itc2Vjb25kYXJ5OiAke3RoZW1lLmNvbG9ycy5jb2xvclNlY29uZGFyeX07XHJcbiAgICAtLWNvbG9yLXNlY29uZGFyeS1jb250cmFzdDogJHt0aGVtZS5jb2xvcnMuY29sb3JTZWNvbmRhcnlDb250cmFzdH07XHJcbiAgICAtLWNvbG9yLWJhY2tncm91bmQ6ICR7dGhlbWUuY29sb3JzLmNvbG9yQmFja2dyb3VuZH07XHJcbiAgICAtLWNvbG9yLWJhY2tncm91bmQtY29udHJhc3Q6ICR7dGhlbWUuY29sb3JzLmNvbG9yQmFja2dyb3VuZENvbnRyYXN0fTtcclxuICB9XHJcblxyXG4gIC8vb3RoZXIgZ2xvYmFsIHN0eWxlcyBnbyBoZXJlLi4uXHJcbiAgcCB7Y29sb3I6Z3JlZW59XHJcbmA7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBHbG9iYWxTdHlsZXM7Il0sInNvdXJjZVJvb3QiOiIifQ==